====================
Mail To 2Do - 2.0.2
====================

Author: James Gibbard (http://jgibbard.me.uk)
Website: http://jgibbard.me.uk/bitbucket/mailto2do

MailTo2Do allows you to easily create tasks in 2Do from your messages in 
Mail. You can choose from a list of custom tags that you can optionally set.

Installation:
--------------
The best way I found to install Applescripts into Mail is to use them as
an OS X Service and assign them to a shortcut key.

1. Download the OS X Service DMG installer (MailTo2Do - http://bit.ly/1eYQr90)
2. Install the service on your computer.
3. Open 'System Preferences', go to Keyboard -> Shortcuts -> Services
4. Select the ‘MailTo2Do’ service; Assign a shortcut key to the service.


Usage:
-------
1. Open Mail.
2. Select a message in the list.
3. Press your chosen shortcut key.
4. Watch your selected message appear in 2Do as a task - and smile :) 